/**
* @file isotphandler.cpp
* @brief This module will handle isotp communications.
* @details This module sends out isotp requests and processes incoming frames
*          to re-assemble the original message
*/

#include "isotphandler.h"

static CS_CONFIG_t *isotp_config;
static ISO_MESSAGE_t isoMessageIncoming; // declare an ISO-TP message
static ISO_MESSAGE_t isoMessageOutgoing; // declare an ISO-TP message
static unsigned long lastMicros;

/**
 * Initializes the isotp subsystem
 */
void isotp_init()
{
	isotp_config = getConfig();
	isotp_reset();
}

/**
 * Resets the isotp state
 */
void isotp_reset()
{
	isoMessageIncoming.id = isoMessageOutgoing.id = 0xffff;
	isoMessageIncoming.length = isoMessageIncoming.index = isoMessageOutgoing.length = isoMessageOutgoing.index = 0;
}

/**
 * the ticker is called in the main loop. It handles the sending of NEXT
 * frames asynchronously
 */
void isotp_ticker()
{
	CAN_frame_t frame; // build the CAN frame
	if (isoMessageOutgoing.flow_active == 0)
		return; //
	if ((micros() - lastMicros) < isoMessageOutgoing.flow_delay)
		return;

	// Prepare the next frame
	frame.FIR.B.FF = isoMessageOutgoing.id < 0x800 ? CAN_frame_std : CAN_frame_ext;
	frame.FIR.B.RTR = CAN_no_RTR;		 // no RTR
	frame.MsgID = isoMessageOutgoing.id; // set the ID
	frame.FIR.B.DLC = 8;				 //command.requestLength + 1;// set the length. Note some ECU's like DLC 8

	frame.data.u8[0] = 0x20 | (isoMessageOutgoing.next++ & 0x0f);
	int i;
	for (i = 0; i < 7 && isoMessageOutgoing.index < isoMessageOutgoing.length; i++)
	{
		frame.data.u8[i + 1] = isoMessageOutgoing.data[isoMessageOutgoing.index++];
	}
	for (; i < 7; i++)
	{
		frame.data.u8[i + 1] = 0;
	}

	// debug
	if (isotp_config->mode_debug & DEBUG_COMMAND_ISO)
	{
		writeOutgoingSerialDebug("> can:Sending ISOTP NEXT:" + canFrameToString(&frame));
	}

	// check if we reached the end of the message
	if (isoMessageOutgoing.length == isoMessageOutgoing.index)
	{
		// Done sending the outgoing message, so reset it and cancel further
		// handling by this ticker
		isoMessageOutgoing.length = isoMessageOutgoing.index = 0;
		isoMessageOutgoing.flow_active = 0;

		// At this moment, further sending by the ticker will stop. A new flow control
		// should not come in, but we now expect the answer, so do not invalidate the
		// incoming id
		// isoMessageIncoming.id = 0xffff;
		// the incoming message is full initiaized (id, index)
	}

	if (isoMessageOutgoing.flow_counter != 0)
	{
		if (--isoMessageOutgoing.flow_counter == 0)
		{
			isoMessageOutgoing.flow_active = 0;
		}
	}

	// send the frame
	led_set(LED_GREEN, true);
	can_send(&frame, 0); // bus logic needs to be added
	led_set(LED_GREEN, false);
	//
	lastMicros = micros();
}

/**
 * Process an incoming ISO-TP frame
 * @param frame Incoming CANbus frame
 * @param bus CANbus (only 0 allowed)* 
 */
void storeIsotpframe(CAN_frame_t *frame, uint8_t bus)
{
	// if there is content and this is the frame we are waiting for
	if (frame->FIR.B.DLC == 0 || frame->MsgID != isoMessageIncoming.id)
	{
		if (isotp_config->mode_debug & DEBUG_BUS_RECEIVE_ISO)
			writeOutgoingSerialDebug("< can:ISO frame of unrequested id:" + String(isoMessageIncoming.id, HEX) + "," + String(frame->MsgID, HEX));
		// new assumption: there is only ONE active ISOTP command going on this means that this condition
		// should reset the ISOTP receiver
		isotp_reset();
		return;
	}

	uint8_t type = frame->data.u8[0] >> 4; // type = first nibble

	// single frame answer ***************************************************
	if (type == 0x0)
	{
		if (isotp_config->mode_debug & DEBUG_BUS_RECEIVE_ISO)
		{
			writeOutgoingSerialDebug("< can:ISO SING:" + canFrameToString(frame));
		}

		uint16_t messageLength = frame->data.u8[0] & 0x0f; // length = second nibble + second byte
		if (messageLength > 7)
		{
			isotp_reset();
			return;
		}
		isoMessageIncoming.length = messageLength;

		// fill up with this initial first-frame data (should always be 6)
		for (int i = 1; i < frame->FIR.B.DLC && isoMessageIncoming.index < isoMessageIncoming.length; i++)
		{
			isoMessageIncoming.data[isoMessageIncoming.index++] = frame->data.u8[i];
		}
		String dataString = isoMessageToString(&isoMessageIncoming);
		if (isotp_config->mode_debug & DEBUG_BUS_RECEIVE_ISO)
			writeOutgoingSerialDebug("> can:ISO MSG:" + dataString);
		if (isotp_config->output_handler)
			isotp_config->output_handler(isoMessageToString(&isoMessageIncoming) + "\n");
		// isoMessageIncoming.id = 0xffff; // cancel this message so nothing will be added until it is re-initialized
		isotp_reset();
		return;
	}

	// first frame of a multi-framed message *********************************
	if (type == 0x1)
	{
		if (isotp_config->mode_debug & DEBUG_BUS_RECEIVE_ISO)
		{
			writeOutgoingSerialDebug("< can:ISO FRST:" + canFrameToString(frame));
		}

		// start by requesting requesing the type Consecutive (0x2) frames by sending a Flow frame
		led_set(LED_GREEN, true);
		can_send_flow(isoMessageOutgoing.id, bus);
		led_set(LED_GREEN, false);

		uint16_t messageLength = (frame->data.u8[0] & 0x0f) << 8; // length = second nibble + second byte
		messageLength |= frame->data.u8[1];
		if (messageLength > 4096)
		{
			writeOutgoingSerialDebug("< can: length FRST > 4096");
			isotp_reset();
			return;
		}

		isoMessageIncoming.length = messageLength;
		for (int i = 2; i < 8; i++)
		{
			isoMessageIncoming.data[isoMessageIncoming.index++] = frame->data.u8[i];
		}
		return;
	}

	// consecutive frame(s) **************************************************
	if (type == 0x2)
	{
		if (isotp_config->mode_debug & DEBUG_BUS_RECEIVE_ISO)
		{
			writeOutgoingSerialDebug("< can:ISO NEXT:" + canFrameToString(frame));
		}

		uint8_t sequence = frame->data.u8[0] & 0x0f;
		if (isoMessageIncoming.next != sequence)
		{
			if (isotp_config->mode_debug & DEBUG_BUS_RECEIVE_ISO)
				writeOutgoingSerialDebug("< can:ISO Out of sequence, resetting");
			isotp_reset();
		}

		for (int i = 1; i < frame->FIR.B.DLC && isoMessageIncoming.index < isoMessageIncoming.length; i++)
		{
			isoMessageIncoming.data[isoMessageIncoming.index++] = frame->data.u8[i];
		}

		// wait for next message, rollover from 15 to 0
		isoMessageIncoming.next = isoMessageIncoming.next == 15 ? 0 : isoMessageIncoming.next + 1;

		// is this the last part?
		if (isoMessageIncoming.index == isoMessageIncoming.length)
		{
			// output the data
			String dataString = isoMessageToString(&isoMessageIncoming);
			if (isotp_config->mode_debug & DEBUG_BUS_RECEIVE_ISO)
				writeOutgoingSerialDebug("< can:ISO MSG:" + dataString);
			if (isotp_config->output_handler)
				isotp_config->output_handler(dataString + "\n");
			isotp_reset();
		}
		return;
	}

	// incoming flow control ***********************************************
	if (type == 0x3)
	{
		if (isotp_config->mode_debug & DEBUG_BUS_RECEIVE_ISO)
			writeOutgoingSerialDebug("< can:ISO FLOW");

		//uint8_t flag = isoMessageIncoming.data[0] &0x0f;
		isoMessageOutgoing.flow_counter = frame->data.u8[1];
		isoMessageOutgoing.flow_delay = frame->data.u8[2] <= 127 ? frame->data.u8[2] * 1000 : frame->data.u8[2] - 0xf0;
		// to avoid overwhelming the outgoing queue, set minimum to 5 ms
		// this is experimental.
		if (isoMessageOutgoing.flow_delay < 5000)
			isoMessageOutgoing.flow_delay = 5000;
		isoMessageOutgoing.flow_active = 1;
		lastMicros = micros();
		return;
	}

	if (isotp_config->mode_debug & DEBUG_BUS_RECEIVE_ISO)
		writeOutgoingSerialDebug("< can:ISO ignoring unknown frame type:" + String(type));
	isotp_reset();
	return;
}

/**
 * Send a flow control frame
 * @param id id ID of the message
 * @param bus CANbus (only 0 allowed)
 */
void can_send_flow(uint32_t id, uint8_t bus)
{
	CAN_frame_t flow;
	flow.FIR.B.FF = id < 0x800 ? CAN_frame_std : CAN_frame_ext;
	flow.FIR.B.RTR = CAN_no_RTR; // no RTR
	flow.MsgID = id;			 // send it to the requestId
	flow.FIR.B.DLC = 8;			 // length 8 bytes
	flow.data.u8[0] = 0x30;		 // type Flow (3), flag Clear to send (0)
	flow.data.u8[1] = 0x00;		 // instruct to send all remaining frames without flow control
	flow.data.u8[2] = 0x00;		 // delay between frames <=127 = millis, can maybe set to 0
	flow.data.u8[3] = 0;		 // fill-up
	flow.data.u8[4] = 0;		 // fill-up
	flow.data.u8[5] = 0;		 // fill-up
	flow.data.u8[6] = 0;		 // fill-up
	flow.data.u8[7] = 0;		 // fill-up
	led_set(LED_GREEN, true);
	can_send(&flow, bus);
	led_set(LED_GREEN, false);
}

/**
 * Send an ISO-TP message
 * @param id ID of the message
 * @param length of the payload
 * @param request payload
 * @param bus CANbus (only 0 allowed)
 */
void requestIsotp(uint32_t id, int16_t length, uint8_t *request, uint8_t bus)
{
	CAN_frame_t frame; // build the CAN frame

	isotp_reset(); // cancel possible ongoing IsoTp run

	isoMessageIncoming.id = id;	  // expected ID of answer
	isoMessageIncoming.index = 0; // starting
	isoMessageIncoming.next = 1;

	if ((isoMessageOutgoing.id = getRequestId(id)) == 0) // ID to send request to
	{
		if (isotp_config->mode_debug & DEBUG_COMMAND)
			writeOutgoingSerialDebug("> can:" + String(id, HEX) + " has no corresponding request ID");
		if (isotp_config->output_handler)
			isotp_config->output_handler(String(id, HEX) + "\n");
		isotp_reset();
		return;
	}
	// store request to send
	isoMessageOutgoing.length = length;
	if (isoMessageOutgoing.length > 4096)
	{
		writeOutgoingSerialDebug("> can:length request > 4096");
		isotp_reset();
		return;
	}

	for (uint16_t i = 0; i < length; i++)
	{
		isoMessageOutgoing.data[i] = request[i];
	}
	isoMessageOutgoing.index = 0; // start at the beginning
	isoMessageOutgoing.next = 1;

	// Prepare the initial frame
	frame.FIR.B.FF = isoMessageOutgoing.id < 0x800 ? CAN_frame_std : CAN_frame_ext;
	frame.FIR.B.RTR = CAN_no_RTR;		 // no RTR
	frame.MsgID = isoMessageOutgoing.id; // set the ID
	frame.FIR.B.DLC = 8;				 //command.requestLength + 1;// set the length. Note some ECU's like DLC 8

	if (isoMessageOutgoing.length <= 7)
	{ // send SING frame
		// prepare the frame
		frame.data.u8[0] = (isoMessageOutgoing.length & 0x0f);
		for (int i = 0; i < isoMessageOutgoing.length; i++)
		{ // fill up the other bytes with the request
			frame.data.u8[i + 1] = isoMessageOutgoing.data[i];
		}
		for (int i = isoMessageOutgoing.length; i < 7; i++)
		{
			frame.data.u8[i + 1] = 0; // zero out frame
		}

		// debug
		if (isotp_config->mode_debug & DEBUG_COMMAND_ISO)
		{
			writeOutgoingSerialDebug("> can:Sending ISOTP SING request:" + canFrameToString(&frame));
		}

		// send the frame
		led_set(LED_GREEN, true);
		can_send(&frame, bus);
		led_set(LED_GREEN, false);

		// --> any incoming frames with the given id will be handled by "storeFrame"
		// and send off if complete. But ensure the ticker doesn't do any flow_block
		// control
		isoMessageOutgoing.length = isoMessageOutgoing.index = 0;
	}

	else
	{ // send a FIRST frame
		// prepare the firt frame
		frame.data.u8[0] = (uint8_t)(0x10 + ((length >> 8) & 0x0f));
		frame.data.u8[1] = (uint8_t)(length & 0xff);
		for (int i = 0; i < 6; i++)
		{ // fill up the other bytes with the first 6 bytes of the request
			frame.data.u8[i + 2] = isoMessageOutgoing.data[isoMessageOutgoing.index++];
		}

		// debug
		if (isotp_config->mode_debug & DEBUG_COMMAND_ISO)
		{
			writeOutgoingSerialDebug("> can:Sending ISOTP FRST request:" + canFrameToString(&frame));
		}

		// send the frame
		led_set(LED_GREEN, true);
		can_send(&frame, bus);
		led_set(LED_GREEN, false);
		// --> any incoming frames with the given id will be handled by "storeFrame" and send off if complete
	}
}

/**
 * Convert a ISO-TP message to readable hex output format, newline terminated
 * @param message Pointer to the ISO_MESSAGE_t struct
 */
String isoMessageToString(ISO_MESSAGE_t *message)
{
	String dataString = String(message->id, HEX) + ",";
	for (int i = 0; i < message->length; i++)
	{
		dataString += getHex(message->data[i]);
	}
	return dataString;
}
